using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PixelCrushers.DialogueSystem;

public class ItemsPicking : MonoBehaviour
{
    void PickUp(DialogueDatabase database)
    {
        for (int i = 0; i < database.items.Count; i++)
        {
            if (database.items[i].fields[0].value == gameObject.name)
            {
                database.items[i].fields[4].value = "True";
                database.SyncAll();
                gameObject.SetActive(false);
            }
        }

        //Debug.Log(database.items[1].fields[4].value);
    }
}
