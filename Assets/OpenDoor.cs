using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    void Open()
    {
        var tags = gameObject.GetComponent<MultiTag>();
        if (tags.HasTag("FutureDoor"))
        {
            transform.position = new Vector3(gameObject.transform.position.x, 6f, gameObject.transform.position.z);
        }
        else if (tags.HasTag("NormalDoor"))
        {
            if (transform.position.x > transform.position.x)
                transform.rotation = Quaternion.Euler(0, 80, 0);
            else
                transform.rotation = Quaternion.Euler(0, -80, 0);
        }
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }
}
