using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class NPCDialogueStart : MonoBehaviour
{

    void StartDialogue()
    {
        var trig = GetComponent<DialogueSystemTrigger>();
        trig.enabled = true;
    }

    void StopCheck()
    {
        var trig = GetComponent<DialogueSystemTrigger>();
        trig.enabled = false;
    }
}
