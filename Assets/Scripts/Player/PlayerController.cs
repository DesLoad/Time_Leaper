using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using PixelCrushers;
using PixelCrushers.DialogueSystem;


using InputDevice = UnityEngine.InputSystem.InputDevice;
using PlayerInputIS = UnityEngine.InputSystem.PlayerInput;


public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;
    private Rigidbody rb;

    [SerializeField] public DialogueDatabase DB;
    private DialogueActor DA;

    #region SpriteLoader
    //���������� ����� �������� ��-�� ����� ���� ����������
    //(� ������ ������ ��������� ��� �� �������, �� ��� �� �������� � ��� ������� ������� � ���������� ��������)

    [SerializeField] List<Sprite> SpriteAsset = new List<Sprite>();
    // ������� 0,1 - ������� �� X Input
    // ������� 2,3 - �������
    
    // 
    //
    private void spriteLoader()
    {
        //Iteract Keyboard = E (Gamepad = Sout(A))
        //Action Keyboard = F (Gamepad = Sout(X))

        if (device.Contains("XInput"))
        {
            iteractButtonSprite.GetComponent<Image>().sprite = SpriteAsset[0]; //A
            actionButtonSprite.GetComponent<Image>().sprite = SpriteAsset[1];  //X
        }
        else if (device.Contains("Keyboard"))
        {
            iteractButtonSprite.GetComponent<Image>().sprite = SpriteAsset[2]; //E
            actionButtonSprite.GetComponent<Image>().sprite = SpriteAsset[3];  //F
        }
    }

    #endregion

    #region input properties
    private PlayerInput _input;
    private PlayerInputIS _inputDevice;
    private float iteractButtonValue;
    [SerializeField] private GameObject iteractButtonSprite;
    private float actionButtonValue;
    [SerializeField] private GameObject actionButtonSprite;
    private string device;
    #endregion

    [SerializeField] private GameObject canvasGO;
    private Canvas canvas;

    private void Awake()
    {
        DA = GetComponent<DialogueActor>();

        _inputDevice = GetComponent<PlayerInputIS>();
        device = _inputDevice.devices[0].ToString();
        _input = new PlayerInput();
        canvas = canvasGO.GetComponent<Canvas>();

        rb = GetComponent<Rigidbody>();
        DontDestroyOnLoad(this.gameObject); //��������� �� ���� ����� ������������� ��� ����� ����, �� ����� ����������

        spriteLoader();
    }

    private void OnEnable()
    {
        _input.Enable();
    }
    private void OnDisable()
    {
        _input.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 direction = _input.Player.Move.ReadValue<Vector2>(); //��������� �������� ������� ��� �������� ������ �������� (��� �� ��������� � ��������� ��������, ���� ���� �� ������ ��������)

        iteractButtonValue = _input.Player.Iteract.ReadValue<float>();
        actionButtonValue = _input.Player.Action.ReadValue<float>();

        if (device != _inputDevice.devices[0].ToString())
        {
            device = _inputDevice.devices[0].ToString();
            spriteLoader();
        }

        Move(direction);
    }

    
    private void Move(Vector2 direction)
    {
        rb.velocity = new Vector3(direction.x * _moveSpeed, 0, 0);
    }

    private void database()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        #region Item/Object

        if (other.gameObject.tag == "PickUpItems")//�������� ���� ������� ��������� � ������
        {
            var but = canvas.transform.Find("Iteract"); //�������� ������ ��������/������� �
            but.gameObject.SetActive(true);

            if (iteractButtonValue == 1)
            {
                other.SendMessage("PickUp", DB);
            }
        }
        if (other.gameObject.tag == "DistractionItems")//������������� � ������������� ��������� �� ���� ������
        {

        }
        #endregion

        #region Iteraction
        //�������� ������ � ������� - �������
        if (other.gameObject.tag == "IteractObject")
        {
            var but = canvas.transform.Find("Iteract"); //�������� ������ ��������/������� �
            but.gameObject.SetActive(true);

            //��� ���� �������� ������� ������ �������, � �� ����������� �������, ���� �������� ���������� � �����
            if (iteractButtonValue == 1)
            {
                but.gameObject.SetActive(false);
                other.gameObject.SetActive(false);
            }
        }
        //NPC
        if (other.gameObject.tag == "NPC")
        {
            var but = canvas.transform.Find("Iteract"); //�������� ������ ��������/������� �
            but.gameObject.SetActive(true);

            if (iteractButtonValue == 1)
            {
                other.GetComponent<DialogueSystemTrigger>().selectedDatabase = DB;
                other.SendMessage("OnUse", this.transform);
            }
        }
        #endregion

        #region Action
        //�������� ������ � ������� - �����
        if (other.gameObject.tag == "UnlockedDoor")
        {
            var but = canvas.transform.Find("Action");
            but.gameObject.SetActive(true);

            //��� ���� �������� ������� ������ �������, � �� ����������� �������, ���� �������� ���������� � �����
            if (actionButtonValue == 1)
            {

                //���������� ����� ������ ������ ����� � Sendmessage
                but.gameObject.SetActive(false);
                other.SendMessage("Open");
            }
        }
        #endregion
    }

    private void OnTriggerExit(Collider other)
    {
        canvas.transform.Find("Action").gameObject.SetActive(false);
        canvas.transform.Find("Iteract").gameObject.SetActive(false);
    }
}
